// pages/global_search/global_search.js

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // blog文章
    blogList:[],

    // 瀑布流色块
    colourList: [{
      colour: 'bg-red'
    }, {
        colour: 'bg-orange'
    }, {
        colour: 'bg-yellow'
    }, {
        colour: 'bg-olive'
    }, {
        colour: 'bg-green'
    }, {
        colour: 'bg-cyan'
    }, {
        colour: 'bg-blue'
    }, {
        colour: 'bg-purple'
    }, {
        colour: 'bg-mauve'
    }, {
        colour: 'bg-pink'
    }, {
        colour: 'bg-lightBlue'
    }],

    // 随机数
    randomNum: 0,
    // 动画时间
    animationTime: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    // 开启/关闭炫图模式
    // console.log("开启/关闭炫图模式skin：",app.globalData.skin)
    this.setData({
      skin: app.globalData.skin
    });

    // allBlogData不为真（为空）
    // if(!app.globalData.allBlogData){
      // 调用云函数查询数据
      wx.cloud.callFunction({
        name: "query",
        data: {
          dbName: "test",
          filter: {
          },
        }
      }).then( res => {
        console.log("global_search.js云函数查询成功回调：",res)
        // console.log("赋值给blogList的数组：",res.result.data)
        this.setData({
          blogList: res.result.data,
        })
        app.globalData.total = res.result.count.total
        // console.log("文章总数：",app.globalData.total)
      })
    // }else{
    //   this.setData({
    //     blogList: app.globalData.allBlogData
    //   })
    // }

  },

  // 跳转到文章详情页并携带index参数
  toBlog(event){
    // console.log("点击了跳转事件：",event)
    let blog_index = event.currentTarget.dataset.index
    // 要传递的对象，传递前数据进行转换
    // var blogItem = JSON.stringify(this.data.blogList[blog_index])
    
    app.globalData.blogItemGlobal = this.data.blogList[blog_index]
    // console.log(app.globalData.blogItemGlobal)
    // 这里blogindex参数没用到，可不传递
    wx.navigateTo({
      url: '../blog/blog?blogindex='+blog_index,
    })
  },

  //获取随机数
  randomNum: function() {
    var num = Math.floor(Math.random() * 10);
    this.setData({
        randomNum: num
    });
  },

  /**
  * 监听屏幕滚动 判断上下滚动
  */
  onPageScroll: function (event) {
    // console.log("屏幕滚动：",event)
    this.setData({
      scrollTop: event.detail.scrollTop
    })
  },

  /**
  * 加载更多
  */
  loadMore: function () {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})