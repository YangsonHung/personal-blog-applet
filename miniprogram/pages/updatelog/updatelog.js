// pages/updatelog/updatelog.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    topImg: "https://6c69-lingdu-test-n59r0-1300335304.tcb.qcloud.la/besidesApplets/swiper/updatelog.jpg?sign=64f733d77587491ad16a6a9245e1c649&t=1599620321",
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    skin: app.globalData.skin,
    loading: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 开启/关闭炫图模式
    // console.log("开启/关闭炫图模式skin：",app.globalData.skin)
    this.setData({
      skin: app.globalData.skin
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})