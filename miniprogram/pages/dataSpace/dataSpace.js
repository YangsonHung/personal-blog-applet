// pages/workshop/workshop.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: ['该页面作者正努力开发中', '点击即可复制网盘下载链接', '敬请期待'],
    toolsList: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.findToolsList()
  },
  // 查询友链列表
  findToolsList(){
    wx.cloud.callFunction({
      name: "query",
      data: {
        dbName: "L-Tools",
        filter: {
        },
      }
    }).then( res => {
      // console.log("dataSpace.js调用云函数查询成功回调：",res)
      this.setData({
        toolsList: res.result.data,
      })
    })
  },
  // 点击复制链接到剪切板
  prevent(event) {
    // wx.setClipboardData 设置系统剪贴板的内容
    // console.log("设置系统剪贴板的内容:",event);
    wx.setClipboardData({
        data: event.currentTarget.dataset.url,
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

})