// pages/blog/blog.js

// 是否已点赞
let love = false

const app = getApp();
// 评论倒计时
var countdown = 60;
let time = require('../../utils/time.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 代码高量样式
    codeStyle: app.globalData.highlightStyle,
    blogItem:{},

    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    CommentShow: false,
    ButtonTimer: '',//  发送评论按钮定时器
    LastTime: 60,
    // 评论开关
    CommentSwitch: false,
    // 输入框中的值（数据，用于评论成功后清除）
    commentValue:'',
    // 控制点赞图标样式
    pageLove: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    // console.log("文章页面用户信息：",app.globalData.userInfo)

    if(love==true){
      this.setData({
        pageLove:true
      })
    }

    // console.log("上一个页面传过来的参数：",options)
    // let blogIndex = options.blogindex
    // console.log("blogItemGlobal:",app.globalData.blogItemGlobal)
    // 转换为对象类型
    // var blogDetail = JSON.parse(options.blogItem)
    this.setData({
      blogItem: app.globalData.blogItemGlobal
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    let param = {
      Views: this.data.blogItem.Views+1
    }
    // 调用云函数修改文章浏览量
    wx.cloud.callFunction({
      name: "update",
      data: {
        dbName: "test",
        param: param,
        filter:{
          _id: this.data.blogItem._id
        }
      }
    }).then( res => {
      // console.log("云函数更新浏览量成功回调：",res)
    })

    // console.warn(app.globalData.userInfo);

    if (app.globalData.userInfo) {
      this.setData({
          userInfo: app.globalData.userInfo,
          hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
          this.setData({
              userInfo: res.userInfo,
              hasUserInfo: true,
          })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
            app.globalData.userInfo = res.userInfo
            this.setData({
                userInfo: res.userInfo,
                hasUserInfo: true
            })
        }
      })
    }

  },

  getUserInfo: function (e) {
    // console.log(e)
    app.globalData.userInfo = e.detail.userInfo;
    // app.globalData.nickName = e.detail.userInfo.nickName;
    // app.globalData.avatarUrl = e.detail.userInfo.avatarUrl;
    this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
    });
  },

  /**
     * 评论开关按钮回调
     */
  successSwitch: function(res, selfObj) {
    var that = this;
    // console.warn(res.data);
    that.setData({
      CommentSwitch: !res.data,
    });
  },
  /**
     * 用户输入的评论(bindinput事件)
     */
  Comment: function (e) {
    // console.log("用户输入的评论：",e)
    var content = e.detail.value.replace(/\s+/g, '');
    // console.log("评论内容",content);
    this.setData({
      CommentContent: content,
    });
    // console.log("评论：",this.data.CommentContent)
  },

  // 发送（提交评论信息）
  CommentSubmit(){
    var that = this;
    if (!that.data.CommentContent) {
      wx.showToast({
        title: '评论内容不能为空！',
        icon: 'none',
        duration: 2000
      })
      // console.error("评论内容为空!");
    } else{
      that.setData({
        CommentShow: true,
      });
      that.data.ButtonTimer = setInterval(function () {
        if (countdown == 0) {
          that.setData({
            CommentShow: false,
          })
          countdown = 60;
          clearInterval(that.data.ButtonTimer);
          return;
        } else {
          that.setData({
            LastTime: countdown
          });
          // console.warn(countdown);
          countdown--;
        }
      }, 1000)
      // 持久化评论
      // 调用云函数修改文章
      // 时间戳
      var timestamp = Date.parse(new Date());
      // 评论参数
      let commentParam = {
        userURL: app.globalData.userInfo.avatarUrl,
        Nickname: app.globalData.userInfo.nickName,
        time: time.customFormatTime(timestamp,'Y-M-D'),
        content_message: that.data.CommentContent
      }
      wx.cloud.callFunction({
        name: "addBlogComment",
        data: {
          dbName: "test",
          param: commentParam,
          filter:{
            _id: this.data.blogItem._id
          }
        }
      }).then( res => {
        // console.log("云函数插入评论成功回调：",res)
        wx.showToast({
          title: "评论成功！",
          duration: 2000
        });
        // 清空输入框
        this.setData({
          commentValue: "",
          CommentContent: ""
        });
        //查询（刷新）评论内容
        wx.cloud.callFunction({
          name: "query",
          data: {
            dbName: "test",
            filter: {
              _id: this.data.blogItem._id
            },
          }
        }).then( res => {
          // console.log("云函数查询成功回调：",res)
          // console.log("赋值给blogItem的评论：",res.result.data[0].comments)
          var up = "blogItem.comments"
          this.setData({
            blogItem:res.result.data[0],
            // [up]: res.result.data[0].comments,
          })
          // console.log("blogItem.comments是否赋值成功：",this.data.blogItem.comments)

        })

      })
    }
  },

  CommentSubmitTips: function() {
    wx.showToast({
      title: this.data.LastTime + "s 后再次评论",
      icon: 'none',
      duration: 1000
    })
  },

  // 点击喜欢
  Likes: function() {

    if(love==false){
      love=true
      let paramLove = {
        Likes: this.data.blogItem.Likes+1
      }
      // 调用云函数修改文章Likes
      wx.cloud.callFunction({
        name: "update",
        data: {
          dbName: "test",
          param: paramLove,
          filter:{
            _id: this.data.blogItem._id
          }
        }
      }).then( res => {
        // console.log("云函数更新点赞量成功回调：",res)
        wx.showToast({
          title: "点赞成功！",
          duration: 2000
        });
        // 动态修改前端Likes的值
        var yLikes = "blogItem.Likes"
        this.setData({
          [yLikes]: this.data.blogItem.Likes+1
        })
      })
    }else{
      wx.showToast({
        title: '不能取消点赞,数据库扛不住！',
        icon: 'none'
      })
    }
    

    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})