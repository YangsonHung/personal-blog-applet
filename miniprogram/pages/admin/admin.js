// pages/admin/admin.js
const app = getApp();
let time = require('../../utils/time.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    adminOpenid: app.globalData.adminOpenid,
    roleFlag: app.globalData.roleFlag,
    // 文章总数
    total: app.globalData.total,
    // 小程序总访问量
    Visits:0,
    // 小程序建立天数
    day:''
  },

  // 到写blog页面
  toWrite(){
    wx.navigateTo({
      url: './write/write',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    this.setData({
      skin: app.globalData.skin,
      roleFlag: app.globalData.roleFlag,
      adminOpenid: app.globalData.adminOpenid,
      // 文章总数
      total: app.globalData.total
    })

    // ======计算小程序建立的天数================
    // 小程序建立时间
    let startTime='2020-09-07'
    // 获取当前时间戳
    var timestamp = Date.parse(new Date());
    // console.log("当前时间：",time.customFormatTime(timestamp,'Y-M-D'))
    let endTime = time.customFormatTime(timestamp,'Y-M-D')

    //日期格式化  2020-09-07 ==》2020/09/07
    var startDate = new Date(startTime.replace(/-/g, "/"));
    var endDate = new Date(endTime.replace(/-/g, "/"));
    // console.log("日期格式化后：",endDate)
    // console.log("转成毫秒数后(即转化为时间戳)：",endDate.getTime())
    // 转成毫秒数后，两个日期相减
    var ms = endDate.getTime() - startDate.getTime();
    console.log("转成毫秒数后，两个日期相减：",ms)
    // 转换成天数
    var days = parseInt(ms / (1000 * 60 * 60 * 24));
    // console.log("转换成天数后：",day)
    this.setData({
      day: days
    })
    // ======计算小程序建立的天数end================

    var that = this;
    // 云函数调用
    wx.cloud.callFunction({
      // 云函数名称
      name: 'get_wx_OpenID',
      // 传给云函数的参数
      data: {
      },
      success(res) {
        // console.log("CloudResult:", res);
        // console.log("openidCloudResult:", res.result.openid);
        that.setData({
          openid: res.result.openid
        });
        if (res.result.openid == that.data.adminOpenid) {
          app.globalData.roleFlag = true;
          that.setData({
            roleFlag: true,
          });
          // console.warn("你是管理员！");
        } else {
          app.globalData.roleFlag = false;
          that.setData({
            roleFlag: false,
          });
          // console.warn("你不是管理员！");
        };
      },
      fail: err => {
      },
    })

    // 查询小程序浏览量
    this.selectVisits();

  },

  selectVisits(){
    wx.cloud.callFunction({
      name: "query",
      data: {
        dbName: "L-Extend",
        filter: {
        },
      }
    }).then( res => {
      // console.log("admin.js调用云函数查询小程序浏览量回调：",res)
      this.setData({
        Visits: res.result.data[0].Visits
      })
      
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})