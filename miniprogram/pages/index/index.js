// miniprogram/pages/index/index.js

//获取App应用实例
const app = getApp()
const jinrishici = require('../../utils/jinrishici.js')
// 四个变量处理左滑关闭抽屉
let touchDotX = 0;//X按下时坐标
let touchDotY = 0;//y按下时坐标
let interval;//计时器
let time = 0;//从按下到松开共多少时间*100

Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    miniProgram: app.globalData.miniProgram,
    //判断小程序的组件在当前版本是否可用
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    hasUserInfo: false,
    userInfo:{},
    openid: '',
    adminOpenid: app.globalData.adminOpenid,
    Role: '游客',
    roleFlag: false,//是否是管理员，在抽屉进入后台管理时使用
    // 轮播图
    swiperList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    // 查询轮播图列表
    wx.cloud.callFunction({
      name: "query",
      data: {
        dbName: "L-Swiper",
        filter: {
        },
      }
    }).then( res => {
      // console.log("index.js调用云函数查询成功回调：",res)
      this.setData({
        swiperList: res.result.data
      })
    })
    
    // 每日诗词
    jinrishici.load(result => {
      this.setData({
          jinrishici: result.data.content
      });
    });

    // 小程序浏览量
    wx.cloud.callFunction({
      name: "query",
      data: {
        dbName: "L-Extend",
        filter: {
        },
      }
    }).then( res => {
      // console.log("index.js调用云函数查询小程序浏览量回调：",res)
      app.globalData.Visits=res.result.data[0].Visits+1
      let param = {
        Visits: app.globalData.Visits
      }
      // 调用云函数修改小程序浏览量
      wx.cloud.callFunction({
        name: "update",
        data: {
          dbName: "L-Extend",
          param: param,
          filter:{
          }
        }
      }).then( res => {
        // console.log("云函数更新小程序浏览量回调：",res)
      })
      
    })

  },

  // 微信授权按钮
  getUserInfo: function (e) {
    // console.log(e.detail.errMsg)
    // console.log(e.detail.userInfo)
    // console.log(e.detail.rawData)
    var that = this;
    if(e.detail.errMsg == "getUserInfo:fail auth deny") {
      that.setData({
        hasUserInfo: false,
        Role: '游客',
        roleFlag: false,
      })
    }else {
      app.globalData.userInfo = e.detail.userInfo;
      that.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })
      var that = this;
      // 云函数调用(获取微信的上下文)
      // 上下文包括小程序appid，微信的 openid，和 unionid
      wx.cloud.callFunction({
        // 云函数名称
        name: 'get_wx_OpenID',
        // 传给云函数的参数
        data: {
        },
        success(res) {
          // console.log("CloudResult云函数回调结果：:", res);
          // console.log("openidCloudResult:", res.result.openid);
          that.setData({
            openid: res.result.openid
          });
          // 如果当前微信用户的 openid 与App全局中设置的adminOpenid 相等，即为管理员
          if (res.result.openid == that.data.adminOpenid) {
            that.setData({
              Role: '管理员',
              roleFlag: true,
            });
            console.warn("你是管理员！");
          } else {
            that.setData({
              Role: '游客',
              roleFlag: false,
            });
            // console.warn("你不是管理员！");
          };
        },
        fail: err => {
          },
      })
    }
   
  },

  // 轮播图点击事件
  swipertoBlog(event){
    
  },

  // 显示抽屉
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  // 隐藏抽屉
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

  // 触摸开始事件(通过判断手势滑动的方向，来关闭抽屉)
  touchStart: function (e) {
    touchDotX = e.touches[0].pageX; // 获取触摸时的原点
    touchDotY = e.touches[0].pageY;
    // 使用js计时器记录时间    
    interval = setInterval(function () {
      time++;
    }, 100);
  },
  // 触摸结束事件
  touchEnd: function (e) {
      let touchMoveX = e.changedTouches[0].pageX;
      let touchMoveY = e.changedTouches[0].pageY;
      let tmX = touchMoveX - touchDotX;
      let tmY = touchMoveY - touchDotY;
      if (time < 20) {
          let absX = Math.abs(tmX);
          let absY = Math.abs(tmY);
          if (absX > 2 * absY) {
              if (tmX < 0) {
                console.log("向左滑动，关闭抽屉！")
                this.setData({
                  modalName: null
                })
              } else {
                // console.log("向右滑动，不关闭抽屉！")
                this.setData({//显示抽屉
                  modalName: "viewModal"
                })
              }
          }
          if (absY > absX * 2 && tmY < 0) {
              console.log("====上滑动=====")
          }
      }
      clearInterval(interval); // 清除计时器setInterval
      time = 0;//重置从按下到松开共多少时间*100
  },

  /**
  * 监听屏幕滚动 判断上下滚动
  */
  onPageScroll: function (event) {
    // console.log("屏幕滚动：",event)
    this.setData({
      scrollTop: event.detail.scrollTop
    })
  },

  /**
  * 加载更多
  */
  loadMore: function () {

  },

  bindGetUserInfo (e) {
    console.log("点击了授权登录：",e)
    console.log(e.detail.userInfo)
  },

  // 点击了抽屉里的GitHub
  showMask: function (e) {
    this.selectComponent("#authorCardId").showMask();
    this.shutDownDrawer();
  },
  // 关闭抽屉  （不是管理员点击后台时也关闭）
  shutDownDrawer: function (e) {
    this.setData({
        modalName: null
    })
  },

  // 开启/关闭炫图模式
  switchSex: function (e) {
    console.warn("开启/关闭炫图模式:",e.detail.value);
    app.globalData.skin = e.detail.value
    console.log("skin：",app.globalData.skin)
    this.setData({
        skin: e.detail.value
    });
  }, 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    if (app.globalData.userInfo) {
      this.setData({
          userInfo: app.globalData.userInfo,
          hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        // console.log("callback之后：",res)
        // console.log("callback之后：",res.userInfo)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true,
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
          success: res => {
              app.globalData.userInfo = res.userInfo
              this.setData({
                  userInfo: res.userInfo,
                  hasUserInfo: true
              })
          }
      })
    }

    var that = this;
      // 云函数调用
      wx.cloud.callFunction({
      // 云函数名称
      name: 'get_wx_OpenID',
      // 传给云函数的参数
      data: {
      },
      success(res) {
        // console.log("CloudResult:", res);
        // console.log("openidCloudResult:", res.result.openid);
        that.setData({
          openid: res.result.openid
        });
        if (res.result.openid == that.data.adminOpenid) {
          app.globalData.roleFlag = true;
          that.setData({
            Role: '管理员',
          });
          if (app.globalData.userInfo) {
            that.setData({
              roleFlag: true,
            });
          }
          // console.warn("你是管理员！");
        } else {
          app.globalData.roleFlag = false;
          that.setData({
            Role: '游客',
            roleFlag: false,
          });
          // console.warn("你不是管理员！");
        };
      },
      fail: err => {
      },
    })
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      // title: this.data.jinrishici,
      title:'编程之外',
      path: '/pages/index/index',
      imageUrl: 'https://6c69-lingdu-test-n59r0-1300335304.tcb.qcloud.la/besidesApplets/%E5%B0%8F%E7%A8%8B%E5%BA%8F.png?sign=4e740fda4ea780918e2816a9c4aed58d&t=1599549691',
    }
  }
})