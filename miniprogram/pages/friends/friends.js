// pages/friends/friends.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    skin: app.globalData.skin,
    loading: true,
    animationTime: 1,
    list: ['☀点击即可复制链接地址', '♧欢迎留言友链', '🎉敬请期待'],

    // 友链列表 Links集合(表)
    LinksList: [],

    colourList: [{
        colour: 'bg-red'
    }, {
        colour: 'bg-orange'
    }, {
        colour: 'bg-yellow'
    }, {
        colour: 'bg-olive'
    }, {
        colour: 'bg-green'
    }, {
        colour: 'bg-cyan'
    }, {
        colour: 'bg-blue'
    }, {
        colour: 'bg-purple'
    }, {
        colour: 'bg-mauve'
    }, {
        colour: 'bg-pink'
    }, {
        colour: 'bg-lightBlue'
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.randomNum();
    // 开启/关闭炫图模式
    // console.log("开启/关闭炫图模式skin：",app.globalData.skin)
    this.setData({
      skin: app.globalData.skin
    });

    this.selectLinksList()
  },

  // 查询友链列表
  selectLinksList(){
    wx.cloud.callFunction({
      name: "query",
      data: {
        dbName: "L-Links",
        filter: {
        },
      }
    }).then( res => {
      // console.log("friends.js调用云函数查询成功回调：",res)
      // console.log("赋值给LinksList的数组：",res.result.data)
      this.setData({
        LinksList: res.result.data,
      })
    })
  },

  //获取随机数
  randomNum: function () {
      var num = Math.floor(Math.random() * 10);
      this.setData({
          randomNum: num
      });
  },

  // 点击复制链接到剪切板
  prevent(event) {
    // wx.setClipboardData 设置系统剪贴板的内容
    // console.log("设置系统剪贴板的内容:",event);
    wx.setClipboardData({
        data: event.currentTarget.dataset.url,
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})