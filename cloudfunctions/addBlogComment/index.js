// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()
// 获取数据库操作符
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  let dbName = event.dbName
  let filter = event.filter ? event.filter : {}
  let param = event.param ? event.param : {}
  
  return db.collection(dbName).where(filter).update({
    // 向集合中comments数组字段添加param对象，
    // 若集合中不存在 comments数组字段，则会自动创建comments字段并且是数组类型
    data:{
      comments:_.push(param)
    }
  })

}