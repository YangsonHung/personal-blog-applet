// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {

  console.log("add云函数event：",event)
  const wxContext = cloud.getWXContext()

  // 传递来的dbName参数（要操作的集合）
  let dbName = event.dbName
  let param = event.param ? event.param : {}
  let flag = false
  
  await db.collection(dbName).add({ data: param }).then(res => {
    flag =  true
  }).catch(res=>{

  })

  return flag

}