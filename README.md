
<details><summary>目录</summary>

- [关于小程序](#关于小程序)
- [特别致谢](#特别致谢)
- [小程序页面](#小程序页面)
- [关于线上版本](#关于线上版本)
- [快速开始](#快速开始)
- [赞赏支持](#赞赏支持)
- [更新日志](#更新日志) 

</details>

## 关于小程序 
&emsp;&emsp;小程序前端使用 **ColorUI 组件库**，并且参考了大佬“**爱敲代码的猫**” 的开源项目 **WeHalo** 的页面设计，后端使用小程序**纯云开发**实现文章、评论、友链等等数据的CRUD。 

&emsp;&emsp;该小程序已上线，微信搜索小程序“**遇见0和1**”，或者直接扫描下方小程序二维码即可看到线上版本

## 特别致谢 

-  **ColorUI：** [https://github.com/weilanwl/ColorUI](https://github.com/weilanwl/ColorUI)

-  **WeHalo：** [https://gitee.com/aquanrun/WeHalo](https://gitee.com/aquanrun/WeHalo)

&emsp;&emsp;感谢ColorUI的开发者们，写出了那么漂亮的小程序组件库，感谢“**爱敲代码的猫**”，开源了非常优秀的WeHalo项目。

---

## 小程序页面
![在这里插入图片描述](images/20200907192615994.png)

---
## 关于线上版本 
&emsp;&emsp;由于个人小程序会被限制“信息发布”等带有用户个人主观行为的内容，为了通过审核，所以线上版本删除了小程序后台的文章发布页面(只有管理员，就我才能访问小程序后台)，所以小程序的主要功能文章发布被限制，现在的操作就是，把文章内容直接插入到云开发的后台数据库中😪😪。

由于小程序后台我使用的纯云开发实现，所以能支撑的数据量等不会太大，也还有一些小bug。

---
## 快速开始 

### 1、集合结构

云开发的四个集合（即表，在小程序云开发中称“集合”）：test，L-Swiper，L-Tools 和 L-Links，分别存储 博客文章、首页轮播图、友链、资源分享页面的数据，tese 集合最为复杂，包括了文章的评论数据，最初用test为集合名称是在写代码测试用，后来懒得改了，四个集合结构如下：

![image-20201017212819139](images/image-20201017212819139.png)

![image-20201017212924844](images/image-20201017212924844.png)

![image-20201017213007341](images/image-20201017213007341.png)

![image-20201017213043784](images/image-20201017213043784.png)

---

### 2、管理员权限

只有管理员才能访问小程序管理后台，编写文章等操作，该小程序的管理员和游客是通过用户的**OpenID**来区别的，用户的OpenID可通过如下云函数代码轻松获得 ，部署时在app.js的全局变量中把 **adminOpenid** 改成你自己的OpenID即可获得管理员权限：

```js
const cloud = require('wx-server-sdk')

cloud.init()

// event 包含了调用端（小程序端）调用该函数时传过来的参数，同时还包含了可以通过 getWXContext 方法获取的用户登录态 `openId` 和小程序 `appId` 信息
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  return {
    event,
    openid: wxContext.OPENID,
    appid: wxContext.APPID,
    unionid: wxContext.UNIONID,
  }
}
```

---

### 3、云开发相关设置

1. 在app.js中，要把 env: 'lingdu-test-n59r0', 这里要改成你自己云开发的环境id；
2. 该小程序共有**五个云函数**，所有的**增删改查**都通过其中四个云函数来完成（小程序端调用云函数的同时，传递给云函数要操作的集合名称以及CRUD的条件），其中一个云函数是获取当前用户的OpenID的，部署时要分别右击选择**创建并部署：云端安装依赖**；
3. 若上一步部署云函数不成功，则需要先执行 **npm install** 安装一下云开发环境的依赖；

---

赞赏支持

熬夜爆肝辛苦，开源不易！若项目对您有帮助，不防Start支持一下吧🤣🤣



## 更新日志

2021.01.03: 重构了资源分享模块

2020.10.17：修复导航栏空白bug，修改资源分享模块设计

2020.09.15：友链、轮播图等数据持久化到数据库

2020.09.07：小程序v1.0正式上线啦 ✨